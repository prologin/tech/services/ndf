from django.urls import path

from .views import (
    ExpenseReportCreateView,
    ExpenseReportDetailView,
    ExpenseReportListView,
    ExpenseReportUpdateView,
    HelpView,
    IndexView,
    UserBankingInformationDetailView,
    UserBankingInformationListView,
    UserBankingInformationUpdateView,
)

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("help/", HelpView.as_view(), name="help"),
    path("ndf/", ExpenseReportListView.as_view(), name="expense-report-list"),
    path(
        "ndf/new/",
        ExpenseReportCreateView.as_view(),
        name="expense-report-create",
    ),
    path(
        "ndf/<int:pk>/",
        ExpenseReportDetailView.as_view(),
        name="expense-report-detail",
    ),
    path(
        "ndf/<int:pk>/edit/",
        ExpenseReportUpdateView.as_view(),
        name="expense-report-update",
    ),
    path(
        "user/banking-information/",
        UserBankingInformationListView.as_view(),
        name="user-banking-information-list",
    ),
    path(
        "user/banking-information/<int:pk>/",
        UserBankingInformationDetailView.as_view(),
        name="user-banking-information-detail",
    ),
    path(
        "user/banking-information/<int:pk>/edit/",
        UserBankingInformationUpdateView.as_view(),
        name="user-banking-information-update",
    ),
]

import magic
import re
from django.core.exceptions import ValidationError


def validate_is_pdf(file):
    valid_mime_types = ["application/pdf"]

    file_mime_type = magic.from_buffer(file.read(1024), mime=True)

    if file_mime_type not in valid_mime_types:
        raise ValidationError("Unsupported file type.")


def validate_document_id(document_id):
    return re.match(r"^NDF[0-9]{4}-[0-9]{3}$", document_id)

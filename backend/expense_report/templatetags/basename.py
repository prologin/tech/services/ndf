import os

from django import template

register = template.Library()


@register.filter()
def basename(obj):
    return os.path.basename(obj.name)

from django import template

register = template.Library()


@register.filter()
def nowrap(obj):
    print(obj)
    mdr = obj.replace("\n", " ").replace("\r", "")
    print(mdr)
    return mdr

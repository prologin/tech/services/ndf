from django import template

register = template.Library()


@register.filter()
def get_transition_display(transition_name):
    displays = {
        "submit": "Soumettre",
        "approve": "Approuver",
        "reject": "Rejeter",
        "reimburse": "Rembourser",
        "close": "Fermer",
        "destroy": "Détruire",
    }
    return displays[transition_name]


@register.filter()
def get_available_transitions(obj, user=None):
    return obj.get_available_transitions(user)

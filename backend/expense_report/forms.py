from django import forms
from django.forms.models import inlineformset_factory

from .models import ExpenseReport, ExpenseReportEntry, UserBankingInformation


class ClearableFileInput(forms.ClearableFileInput):
    template_name = "clearable_file_input.html"


class ExpenseReportForm(forms.ModelForm):
    class Meta:
        model = ExpenseReport
        fields = (
            "approved_by",
            "event",
            "event_description",
            "description",
        )
        labels = {
            "approved_by": "Approuvé par",
            "event": "Événement",
            "event_description": "Complément d'événement",
            "description": "Descriptif",
        }
        help_texts = {
            "approved_by": "Veuillez sélectionner le département ayant approuvé vos achats.",
            "event": "Veuillez sélectionner l'événement associé à cette note de frais.",
            "event_description": "Pour GCC! et les épreuves régionales, veuillez indiquer l'année, le mois et le lieu de l'événement. Pour la finale Prologin, indiquez l'année. Sinon, précisez.",
            "description": "Veuillez préciser le but de cette dépense.",
        }
        widgets = {
            "event_description": forms.Textarea(
                attrs={
                    "rows": 3,
                    "placeholder": "Pour GCC!, Ateliers et ER, veuillez indiquer l'année, le mois et le lieu de l'événement. Pour la finale Prologin, indiquez l'année. Sinon, précisez.",
                }
            ),
            "description": forms.Textarea(
                attrs={
                    "rows": 3,
                    "placeholder": "Veuillez préciser le but de cette dépense.",
                }
            ),
        }


class ExpenseReportEntryForm(forms.ModelForm):
    class Meta:
        model = ExpenseReportEntry
        fields = (
            "expense_report",
            "date",
            "expense_type",
            "description",
            "amount",
            "proof",
        )
        labels = {
            "date": "Date de l'achat",
            "expense_type": "Type",
            "description": "Description",
            "amount": "Montant",
            "proof": "Justificatif de paiement",
        }
        help_texts = {
            "date": "Cette date doit être la même que sur le justificatif associé.",
            "expense_type": "Veuillez choisir le type de cette dépense.",
            "description": "Veuillez décrire la dépense. S'il s'agit d'un repas, veuillez inclure le nombre de participants, le nombre d'organisateurs et la liste des organisateurs.",
            "amount": "Ce montant doit être le même que sur le justificatif associé, toutes taxes comprises (TTC).",
            "proof": "PDF uniquement. Doit être un justificatif comptable valable, à savoir une facture. Un ticket de carte bancaire n'est pas un justificatif. Si vous n'êtes pas en possession de justificatif, merci de vous référer à la page d'aide.",
        }
        widgets = {
            "date": forms.DateInput(format="%Y-%m-%d", attrs={"type": "date"}),
            "description": forms.Textarea(
                attrs={
                    "rows": 3,
                    "placeholder": "S'il s'agit d'un repas, veuillez inclure le nombre de participants, le nombre d'organisateurs et la liste des organisateurs.",
                }
            ),
            "proof": ClearableFileInput(
                attrs={"accept": ".pdf,application/pdf"}
            ),
        }


ExpenseReportEntryFormSet = inlineformset_factory(
    ExpenseReport,
    ExpenseReportEntry,
    form=ExpenseReportEntryForm,
    fields=("date", "expense_type", "description", "amount", "proof"),
    extra=1,
    can_delete=True,
)


class ExpenseReportEntryWithAccountingForm(ExpenseReportEntryForm):
    class Meta(ExpenseReportEntryForm.Meta):
        fields = ExpenseReportEntryForm.Meta.fields + (
            "account_dest",
            "account_source",
        )
        labels = {
            **ExpenseReportEntryForm.Meta.labels,
            "account_dest": "Compte destination",
            "account_source": "Compte source",
        }
        help_texts = {
            **ExpenseReportEntryForm.Meta.help_texts,
            "account_dest": "Généralement un compte de charges (6).",
            "account_source": "Généralement un compte de tiers (4).",
        }


ExpenseReportEntryWithAccountingFormSet = inlineformset_factory(
    ExpenseReport,
    ExpenseReportEntry,
    form=ExpenseReportEntryWithAccountingForm,
    fields=ExpenseReportEntryFormSet.form.Meta.fields
    + ("account_dest", "account_source"),
    extra=0,
    can_delete=False,
)


class UserBankingInformationForm(forms.ModelForm):
    class Meta:
        model = UserBankingInformation
        fields = ("file", "iban", "bic")
        labels = {
            "file": "RIB/IBAN au format PDF",
            "iban": "IBAN",
            "bic": "BIC",
        }
        widgets = {
            "file": ClearableFileInput(
                attrs={"accept": ".pdf,application/pdf"}
            ),
        }

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.validated = False
        if commit:
            instance.save()
        return instance


class UserBankingInformationWithValidationForm(forms.ModelForm):
    class Meta(UserBankingInformationForm.Meta):
        fields = UserBankingInformationForm.Meta.fields + ("validated",)

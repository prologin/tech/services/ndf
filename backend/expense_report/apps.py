from django.apps import AppConfig


class ExpenseReportConfig(AppConfig):
    name = "expense_report"

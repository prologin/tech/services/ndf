import auto_prefetch
from auditlog.models import AuditlogHistoryField
from auditlog.registry import auditlog
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator
from django.db import models
from django.db.models import Q, Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import get_template
from django.urls import reverse
from django.utils import timezone
from django.utils.module_loading import import_string
from django_fsm import FSMField, transition
from localflavor.generic.countries.sepa import IBAN_SEPA_COUNTRIES
from localflavor.generic.models import BICField, IBANField
from model_utils.choices import Choices
from model_utils.fields import MonitorField
from model_utils.models import TimeStampedModel
from post_office import mail

from .tasks import expense_report_generate_document
from .validators import validate_is_pdf

User = get_user_model()


class ExpenseReport(auto_prefetch.Model, TimeStampedModel):
    history = AuditlogHistoryField()

    state = FSMField(default="draft")
    state_changed = MonitorField(monitor="state")

    created_by = auto_prefetch.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        db_index=True,
        related_name="created_by",
    )
    modified_by = auto_prefetch.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        db_index=True,
        related_name="last_modified_by",
    )
    submitted_at = models.DateTimeField(blank=True, null=True)

    # Values from https://gitlab.com/prologin/asso/documents/-/blob/main/note_de_frais/schema.json#L18
    APPROVER = Choices(
        ("PRESIDENCY", "Présidence"),
        ("SECRETARY", "Secrétariat"),
        ("TREASURY", "Trésorerie"),
    )
    approved_by = models.CharField(max_length=10, choices=APPROVER)

    # Values from https://gitlab.com/prologin/asso/documents/-/blob/main/note_de_frais/schema.json#L29
    EVENT = Choices(
        ("GCC", "GCC!"),
        ("ATELIER", "Atelier"),
        ("ER", "Épreuve régionale"),
        ("FINAL", "Finale Prologin"),
        ("OTHER", "Autre"),
    )
    event = models.CharField(max_length=10, choices=EVENT)
    event_description = models.TextField()

    description = models.TextField()

    document = models.FileField(
        storage=import_string(settings.PRIVATE_FILE_STORAGE),
        upload_to="expense_report/expense_reports/",
        validators=(
            FileExtensionValidator(allowed_extensions=("pdf",)),
            validate_is_pdf,
        ),
        null=True,
        blank=True,
    )
    document_id = models.CharField(
        max_length=32,
        unique=True,
        blank=True,
        null=True,
    )
    reason = models.TextField(blank=True)

    class Meta(auto_prefetch.Model.Meta):
        ordering = (
            "-submitted_at",
            "-created",
        )
        permissions = (
            ("manage_expensereport", "Can approve an expense report"),
        )

    def __str__(self):
        return f"{self.submitted_at.date() if self.submitted_at else self.created.date()} - {self.created_by} - {self.state} - {self.event}"

    def get_state_display(self):
        displays = {
            "draft": "Brouillon",
            "submitted": "Soumis",
            "approved": "Approuvée",
            "reimbursed": "Remboursée",
            "_closed": "Fermée",
        }
        return displays[self.state]

    def get_absolute_url(self):
        return reverse("expense-report-detail", kwargs={"pk": self.pk})

    def get_absolute_url_full(self):
        return f"{'https' if settings.SECURE_SSL_REDIRECT else 'http'}://{settings.DEFAULT_DOMAIN}{self.get_absolute_url()}"

    @property
    def amount(self):
        return float(self.entries.aggregate(Sum("amount"))["amount__sum"] or 0)

    def department_from_event(self):
        event_to_department = {
            "GCC": "gcc",
            "ATELIER": "ateliers",
            "ER": "concours",
            "FINAL": "concours",
            "OTHER": "FIXME",
        }
        return event_to_department.get(self.event, "FIXME")

    def accounting_entries(self):
        if (
            self.entries.filter(
                Q(account_source=None) | Q(account_dest=None)
            ).count()
            != 0
        ):
            return None

        context = {
            "object": self,
            "accounts_dest": self.entries.values("account_dest").annotate(
                amount=Sum("amount")
            ),
        }
        return get_template("accounting_entries.plain").render(context)

    @transition(field=state, source="draft", target="submitted")
    def submit(self):
        self.submitted_at = timezone.now()
        self.reason = ""

        mail.send(
            recipients=settings.EXPENSE_REPORTS_MANAGERS,
            subject="[NDF] Une nouvelle note de frais a été soumise.",
            message=get_template("mail/submitted.plain").render(
                {"object": self}
            ),
        )

    @transition(
        field=state,
        source="submitted",
        target="approved",
        permission="expense_report.manage_expensereport",
    )
    def approve(self, document_id):
        self.document_id = document_id
        self.reason = ""

        mail.send(
            recipients=self.created_by.email,
            subject="[NDF] Ta note de frais a été approuvée.",
            message=get_template("mail/approved.plain").render(
                {"object": self}
            ),
        )

        expense_report_generate_document.delay(self.pk)

    @transition(
        field=state,
        source="submitted",
        target="draft",
        permission="expense_report.manage_expensereport",
    )
    def reject(self, reason):
        self.reason = reason

        mail.send(
            recipients=self.created_by.email,
            subject="[NDF] Ta note de frais a été rejetée.",
            message=get_template("mail/rejected.plain").render(
                {"object": self}
            ),
        )

    @transition(
        field=state,
        source="approved",
        target="reimbursed",
        permission="expense_report.manage_expensereport",
    )
    def reimburse(self):
        self.reason = ""

        mail.send(
            recipients=self.created_by.email,
            subject="[NDF] Ta note de frais a été remboursée.",
            message=get_template("mail/reimbursed.plain").render(
                {"object": self}
            ),
        )

    @transition(
        field=state,
        source="reimbursed",
        target="_closed",
        permission="expense_report.manage_expensereport",
    )
    def close(self):
        self.reason = ""

    @transition(field=state, source="draft", target="*")
    def destroy(self):
        self.delete()

    def get_available_transitions(self, user=None):
        if not user:
            transitions = self.get_available_state_transitions()
        else:
            transitions = self.get_available_user_state_transitions(user)
        return [t.name for t in transitions]


class ExpenseReportEntry(auto_prefetch.Model):
    history = AuditlogHistoryField()

    expense_report = auto_prefetch.ForeignKey(
        ExpenseReport,
        on_delete=models.CASCADE,
        db_index=True,
        related_name="entries",
    )

    date = models.DateField()

    # Values from https://gitlab.com/prologin/asso/documents/-/blob/main/note_de_frais/schema.json#L73
    EXPENSE_TYPES = Choices(
        ("TRANSPORT", "Transport"),
        ("ACCOMODATION", "Hébergement"),
        ("MEAL", "Repas"),
        ("PURCHASE", "Achat"),
        ("OTHER", "Autre"),
    )
    expense_type = models.CharField(max_length=12, choices=EXPENSE_TYPES)

    description = models.TextField()

    amount = models.DecimalField(max_digits=7, decimal_places=2)

    proof = models.FileField(
        storage=import_string(settings.PRIVATE_FILE_STORAGE),
        upload_to="expense_report/expense_report_entry_proof/",
        validators=(
            FileExtensionValidator(allowed_extensions=("pdf",)),
            validate_is_pdf,
        ),
    )

    account_source = models.CharField(max_length=200, blank=True, null=True)
    account_dest = models.CharField(max_length=200, blank=True, null=True)

    class Meta(auto_prefetch.Model.Meta):
        ordering = ("date", "expense_type", "amount")

    def __str__(self):
        return f"{self.expense_report} - Entry - {self.date} - {self.expense_type} - {self.amount}"


class UserBankingInformation(auto_prefetch.Model):
    history = AuditlogHistoryField()

    user = auto_prefetch.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name="banking_information",
        primary_key=True,
    )

    file = models.FileField(
        storage=import_string(settings.PRIVATE_FILE_STORAGE),
        upload_to="expense_report/banking_informations/",
        validators=(
            FileExtensionValidator(allowed_extensions=("pdf",)),
            validate_is_pdf,
        ),
        null=True,
    )

    iban = IBANField(include_countries=IBAN_SEPA_COUNTRIES, null=True)
    bic = BICField(null=True)

    validated = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user} - {self.iban} - {self.bic}"

    @property
    def is_complete(self):
        return self.file and self.iban and self.bic


@receiver(post_save, sender=User)
def create_user_banking_information(sender, instance, created, **kwargs):
    if created:
        bi = UserBankingInformation.objects.create(user=instance)
        bi.save()


auditlog.register(
    ExpenseReport,
    exclude_fields=["created", "modified"],
    serialize_data=True,
    serialize_auditlog_fields_only=True,
)
auditlog.register(
    ExpenseReportEntry,
    serialize_data=True,
    serialize_auditlog_fields_only=True,
)
auditlog.register(
    UserBankingInformation,
    serialize_data=True,
    serialize_auditlog_fields_only=True,
)

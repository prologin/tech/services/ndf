from io import BytesIO

import requests
from celery import shared_task
from django.apps import apps
from django.conf import settings
from django.core.files.base import ContentFile
from django.db import transaction
from django_utils.auth.utils import get_auth_headers_for_app
from pypdf import PdfWriter


@shared_task(
    acks_late=True,
    autoretry_for=(Exception,),
    max_retries=5,
    retry_backoff=True,
)
def expense_report_generate_document(expense_report_pk):
    e = apps.get_model("expense_report.ExpenseReport").objects.get(
        pk=expense_report_pk
    )

    data = {
        "name": f"{e.created_by.first_name} {e.created_by.last_name}",
        "date": e.submitted_at.date().isoformat(),
        "approved_by": e.get_approved_by_display(),
        "event": e.get_event_display(),
        "event_description": e.event_description,
        "description": e.description,
    }

    expenses = []
    for entry in e.entries.all():
        expense = {
            "date": entry.date.isoformat(),
            "expense_type": entry.get_expense_type_display(),
            "description": entry.description,
            "amount": float(entry.amount),
        }
        expenses.append(expense)
    data["expenses"] = expenses

    response = requests.post(
        settings.DOCUMENTS_GENERATOR_EXPENSE_REPORT_ENDPOINT,
        json=data,
        headers=get_auth_headers_for_app("documents-generator"),
        timeout=60,
    )
    response.raise_for_status()

    merger = PdfWriter()
    merger.append(BytesIO(response.content))

    for entry in e.entries.all():
        merger.append(entry.proof)

    b = BytesIO()
    merger.write(b)
    b.seek(0)

    with transaction.atomic():
        e = apps.get_model("expense_report.ExpenseReport").objects.get(
            pk=expense_report_pk
        )
        e.document = ContentFile(b.read(), "expense_report.pdf")
        e.save()

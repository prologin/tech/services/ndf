from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.decorators import login_required

from .models import ExpenseReport, ExpenseReportEntry, UserBankingInformation

User = get_user_model()

admin.site.login = login_required(admin.site.login)


class ExpenseReportEntryInline(admin.TabularInline):
    model = ExpenseReportEntry


@admin.register(ExpenseReport)
class ExpenseReportAdmin(admin.ModelAdmin):
    list_filter = ("created_by", "approved_by", "event", "state")
    list_display = list_filter + ("created",)
    list_display_links = list_display
    search_fields = list_display + (
        "event_description",
        "description",
        "reason",
    )
    inlines = (ExpenseReportEntryInline,)
    date_hierarchy = "created"


class UserBankingInformationInline(admin.StackedInline):
    model = UserBankingInformation
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (UserBankingInformationInline,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
